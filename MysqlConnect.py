import pymysql


CONNECTION_INFO = {
    "host": "www.ninggeserver.cn",
    "database": "github",
    "user": "remote",
    "password": "remote666"
}


# get a mysql connection instance
def get_connection():
    return pymysql.connect(host=CONNECTION_INFO.get("host"),
                           user=CONNECTION_INFO.get("user"),
                           passwd=CONNECTION_INFO.get("password"),
                           database=CONNECTION_INFO.get("database"))


# get projects from database
def get_projects(connection=None, project_id=None):
    if connection is None:
        connection = get_connection()

    with connection.cursor() as cursor:
        if project_id is None:
            sql = 'select * from project'
        else:
            sql = 'select * from project where id =' + str(project_id)

        cursor.execute(sql)

        line = cursor.fetchone()

        results = list()
        while line:
            results.append(line)
            line = cursor.fetchone()

        return results


# get commits belong to some project from database
def get_commits_by_pid(project_id, connection=None, limit=None):
    if connection is None:
        connection = get_connection()

    sql = 'select * from commits where pid = ' + str(project_id)

    with connection.cursor() as cursor:
        if limit is not None:
            sql += ' limit ' + str(limit)

        cursor.execute(sql)

        line = cursor.fetchone()

        results = list()
        while line:
            results.append(line)
            line = cursor.fetchone()

        return results


# get commits from database and  support split all works to some ones
def get_commits_by_interval(interval, number, limit, from_id=0, connection=None):
    if connection is None:
        connection = get_connection()

    sql = "SELECT * FROM commits where commits.id % {} = {} limit {}, {}".format(interval, number, from_id, limit)

    with connection.cursor() as cursor:
        cursor.execute(sql)
        connection.commit()

        line = cursor.fetchone()
        results = list()
        while line:
            results.append(line)
            line = cursor.fetchone()

        return results


# update one commit's error message
def update_commit(cid, connection=None):
    if connection is None:
        connection = get_connection()

    sql = "update commits set has_error = 1 where id = {} ".format(cid)

    with connection.cursor() as cursor:

        cursor.execute(sql)
        connection.commit()

        if cursor.rowcount != 1:
            print('error on update {}'.format(cid))


# close a mysql connection
def close(connection):
    connection.close()
