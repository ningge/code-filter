import MysqlConnect
import Judge
import threading


local = threading.local()


def process_commit_message(total_tasks, number, each_works, round):
    connection = local.connection

    records = MysqlConnect.get_commits_by_interval(total_tasks,  number, each_works,
                                                   round * each_works, connection)

    print('this is thread {}'.format(threading.current_thread().name))
    for record in records:
        if Judge.judge_commit(record[2]):
            MysqlConnect.update_commit(record[0], connection)

    if len(records) > 0:
        process_commit_message(total_tasks,  number, each_works,
                                     round + 1)
    else:
        print("thread {} is complete". format(threading.current_thread().name))


def process_local(total_tasks, number, each_works, round, connection):

    local.connection = connection
    process_commit_message(total_tasks, number, each_works, round)
