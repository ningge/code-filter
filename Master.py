import threading
import Tasks
import MysqlConnect


def commit_message_tasks():
    threads_numbers = 8
    tasks = list()

    for i in range(threads_numbers):
        connection = MysqlConnect.get_connection()

        t = threading.Thread(target=Tasks.process_local, args=(threads_numbers, i, 1000, 0, connection), name=str(i))
        tasks.append(t)

    for i in range(len(tasks)):
        tasks[i].start()

    for i in range(len(tasks)):
        tasks[i].join()


commit_message_tasks()