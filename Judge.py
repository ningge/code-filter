import requests
from bs4 import BeautifulSoup
import re

# This file provide judge function to judge if one commit may contains DroidLeaks


# The Android release resource API we concerned
rule = re.compile(r"\.(close|release|removeUpdates|unlock|stop|abandonAudioFocus|cancel|"
                  r"disableNetwork|stopPreview|stopFaceDetection|unregisterListener)\(\s*\)")

# The sensitive words may appear in commit message
rule2 = re.compile(r"(leak|leakage|release|recycle|cancel|unload|unlock|unmount|unregister|close)")
# url prefix for the crawler
URL_PREFIX = "http://www.github.com"


# judge code additions in code diffs
def judge_additions(url):
    response = requests.get(url)

    if response.status_code == 200:
        html = BeautifulSoup(response.text, 'html.parser')

        additions = html.findAll('td', {"class": "blob-code-addition"})

        for addition in additions:
            # print(judge(addition.get_text()))
            if judge(addition.get_text()):
                return True

        fragments = html.find_all('include-fragment', {"class": "diff-progressive-loader"})
        if judge_fragments(fragments):
            return True

        fragments = html.find_all('include-fragment', {"class": "js-diff-entry-loader"})
        if judge_fragments(fragments):
            return True

        return False


def judge_fragments(fragments):
    for fragment in fragments:
        try:
            src = fragment.attrs['src']
        except KeyError:
            # TODO exception handler function
            pass

        sub_result = judge_additions(URL_PREFIX + src)
        if sub_result:
            return sub_result

    return False


# judge if a line of code has the ApiS we care about
def judge(snippet):
    global rule
    if rule.search(snippet) is not None:
        return True
    return False


# judge if commit message contains the words wo want
def judge_commit(message):
    global rule2
    if rule2.search(message) is not None:
        return True
    return False









